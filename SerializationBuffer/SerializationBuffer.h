#pragma once

/* ***************************************
[SerializationBuffer]
수정 : 2018/03/15

example)

{
	SerializationBuffer buffer;
	int src, dest;

	buffer << src;
	buffer >> dest;
}

void ExampleIN(SerializationBuffer* const buffer, int src1, short src2)
{
	(*buffer) << src1;
	(*buffer) << src2;
}

void ExampleOut(SerializationBuffer* const buffer, int* dest1, short* dest2)
{
	(*buffer) >> *dest1;
	(*buffer) >> *dest2;
}


**************************************** */

class SerializationBuffer
{
public:
	enum class eSerializationBuffer
	{
		BUFFER_DEFAULT = 1024
	};

	SerializationBuffer(int size = static_cast<int>(eSerializationBuffer::BUFFER_DEFAULT));
	~SerializationBuffer();

	void	Release();
	void	Clear();
	int		GetBufferSize() const;
	int		GetDataSize() const;
	char*	GetBufferPtr() const;
	int		MoveWritePos(int size);
	int		MoveReadPos(int size);
	int		GetData(char* dest, int size);
	int		PutData(char* src, int size);

	SerializationBuffer& operator =	 (const SerializationBuffer& copy);

	SerializationBuffer& operator << (char					value);
	SerializationBuffer& operator << (unsigned char			value);
	SerializationBuffer& operator << (short					value);
	SerializationBuffer& operator << (unsigned short		value);
	SerializationBuffer& operator << (int					value);
	SerializationBuffer& operator << (unsigned int			value);
	SerializationBuffer& operator << (long					value);
	SerializationBuffer& operator << (unsigned long			value);
	SerializationBuffer& operator << (float					value);
	SerializationBuffer& operator << (double				value);
	SerializationBuffer& operator << (__int64				value);
	SerializationBuffer& operator << (unsigned __int64		value);
	SerializationBuffer& operator << (wchar_t*				value);
	
	SerializationBuffer& operator >> (char&					value);
	SerializationBuffer& operator >> (unsigned char&		value);
	SerializationBuffer& operator >> (short&				value);
	SerializationBuffer& operator >> (unsigned short&		value);
	SerializationBuffer& operator >> (int&					value);
	SerializationBuffer& operator >> (unsigned int&			value);
	SerializationBuffer& operator >> (float&				value);
	SerializationBuffer& operator >> (double&				value);
	SerializationBuffer& operator >> (__int64&				value);
	SerializationBuffer& operator >> (unsigned __int64&		value);
	SerializationBuffer& operator >> (wchar_t*				value);

private:
	char*	_buffer;
	int		_size;
	int		_front;
	int		_rear;
};